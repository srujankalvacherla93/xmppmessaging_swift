//
//  chatRoomMessageTableViewCell.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd. on 17/10/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import UIKit

class chatRoomMessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblOfMessage: UILabel!
    @IBOutlet weak var bubbleViewBackground: UIView!
    @IBOutlet weak var bubbleViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bubbleViewLeadingConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblOfMessage.numberOfLines = 0
        bubbleViewBackground.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        bubbleViewBackground.layer.cornerRadius = 6.0
        self.backgroundColor = UIColor.clear
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
