//
//  UserDefaults.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd on 18/09/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import UIKit

class UserDefaultStore: NSObject {
    
    class var isLogined : String! {
        get{
            if let did = UserDefaults.standard.object(forKey: "isLogined"){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: "isLogined")
        }
    }

    /*
    class var userDetails : UserObject {
        get{
            if let did = UserDefaults.standard.object(forKey: "userDetails"){
                let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserObject
                return did as! UserObject
            }
            return nil
        }
        set(newVal){
            let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: userDetails)
            UserDefaults.standard.set(newVal, forKey: "userDetails")
        }
    }
   */
    class var userName : String! {
        get{
            if let did = UserDefaults.standard.object(forKey: "userName"){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: "userName")
        }
    }
    class var userJid : String! {
        get{
            if let did = UserDefaults.standard.object(forKey: "userJid"){
                return did as? String
            }
            return nil
        }
        set(newVal){
            UserDefaults.standard.set(newVal, forKey: "userJid")
        }
    }


}
