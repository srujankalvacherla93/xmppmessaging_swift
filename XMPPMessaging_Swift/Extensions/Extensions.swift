//
//  Extensions.swift
//  XMPPMessaging_Swift
//
//  Created by Srujan k on 19/10/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func scrollToBottom(){
        DispatchQueue.main.async {
            if self.numberOfSections > 0{
                let indexPath = IndexPath(
                    row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                    section: self.numberOfSections - 1)
                self.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    func scrollToTop() {
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}
