//
//  ChatListViewController.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd on 12/09/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import UIKit
import XMPPFramework

class ChatRoomViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, XMPPStreamDelegate, NSFetchedResultsControllerDelegate {
    
    let CellID = "ID"
    var contact: Contact?
    var arrOfMessages: [ChatMessage] = Array()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//    XMPPvCardTemp *vCard =[[self appDelegate].xmppvCardTempModule vCardTempForJID:[self appDelegate].xmppStream.myJID.bareJID shouldFetch:YES];

    @IBOutlet weak var lblOfState: UILabel!
    @IBOutlet weak var imgOfUser: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tfOfMessage: UITextField!
    @IBOutlet weak var contactName: UILabel!
    @IBOutlet weak var viewofMessage: UIView!
    @IBOutlet weak var bottomContraintOfViewOfMessage: NSLayoutConstraint!
    
    // MARK: - Controller Cyclce
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let photoData: Data? = self.appDelegate.xmppvCardAvatarModule?.photoData(for: XMPPJID(string: contact?.jid!)) ?? Data()
        let photoData: Data? = self.appDelegate.xmppvCardAvatarModule?.photoData(for: XMPPJID(string: (contact?.jid)!)!)
        if photoData != nil{
            imgOfUser.image = UIImage(data: photoData ?? Data())
             self.contact?.pic = UIImage(data: photoData ?? Data())
        }else{
            imgOfUser.image = UIImage.init(named: "Default_ProfileCharacter")
        }
        self.contactName.text = contact?.name
        self.navigationController?.navigationBar.isHidden = true
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = false
        }
        self.appDelegate.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        do{
            try self.fetchResultsController.performFetch()
        }catch{
            print("Fetch Failed")
        }
        // setup tableview
        tableView.register(ChatMessageCell.self, forCellReuseIdentifier: CellID)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = UIColor(white: 0.95, alpha: 1)

    }
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.scrollToBottom()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.appDelegate.xmppStream.removeDelegate(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK: - Table View Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        //        return 1
        return self.fetchResultsController.sections?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return arrOfMessages.count
        if let sections = self.fetchResultsController.sections{
            if section < sections.count{
                let sectionInfo: NSFetchedResultsSectionInfo = (sections[section])
                return sectionInfo.numberOfObjects
            }
            return 0
        }else{
            return 0
        }
        //        return self.fetchResultsController().sections?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        let message = self.fetchResultsController.object(at: indexPath) as? XMPPMessageArchiving_Message_CoreDataObject
//        if (message?.isComposing)!{
//            return UITableViewCell()
//        }else{
//            cell?.textLabel?.text = message?.body
//            print("\(String(describing: message?.composing))")
//            cell?.detailTextLabel?.text = "\(String(describing: message?.isOutgoing))"
//            return cell!
//        }
        //        if !(message?.isComposing)!{
        //            cell?.textLabel?.text = message?.body
        //            print("\(String(describing: message?.composing))")
        //            cell?.detailTextLabel?.text = "\(String(describing: message?.isOutgoing))"
        //            return cell!
        //        }
        //        cell?.textLabel?.text = message.message
        //        cell?.detailTextLabel?.text = "By " + message.sender
        //        return cell!
        
        
        //        cell.loadChatMessage(chatMessage: msg)
//        cell.chatMessage = msg

//        cell = chatRoomMessageTableViewCell.init(style: .default, reuseIdentifier: "")
//        cell.lblOfMessage.text = message?.body
        //
//        let cell = tableView.dequeueReusableCell(withIdentifier: "chatRoomMessageTableViewCell") as! chatRoomMessageTableViewCell
        let msg = ChatMessage(message: message?.body, sender: "asdf",timestamp: message?.timestamp, isOutgoing: message?.isOutgoing, isComposing: message?.isComposing)
//        cell.textLabel?.text = msg.message
//        if msg.isOutgoing!{
//            cell.bubbleViewLeadingConstraint.isActive = true
//            cell.bubbleViewTrailingConstraint.isActive = false
//        }else{
//            cell.bubbleViewLeadingConstraint.isActive = false
//            cell.bubbleViewTrailingConstraint.isActive = true
//        }
        //
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellID, for: indexPath) as? ChatMessageCell
        cell?.chatMessage = msg
        return cell!
    }
    //MARK: - UITextView Delegates
    private func textViewDidBeginEditing(_ textView: UITextView) {
        animateViewMoving(up: true, moveValue: 100)
    }
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        animateViewMoving(up: false, moveValue: 100)
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {        
        resingTf()
        return true
    }
    func animateViewMoving (up:Bool, moveValue :CGFloat){
        let movementDuration:TimeInterval = 0.3
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        UIView.beginAnimations( "animateView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(movementDuration )
        self.view.frame = view.frame.offsetBy(dx: 0, dy: movement)
        //  self.view.frame = CGRectOffset(self.view.frame, 0,  movement)
        UIView.commitAnimations()
    }
    //MARK: - XMPP Delegates
    func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        if (message.chatState != nil){
            self.lblOfState.text =  message.chatStateValue ?? "Unknown"
        }
        if message.body != nil && (message.fromStr?.contains(contact?.name ?? ""))!{
            //            arrOfMessages.append(ChatMessage(message: message.body!, sender: (contact?.name)!, date: Date().xmppDateTimeString, isIncoming: true))
            //            self.tableView.reloadData()
        }
    }
    func xmppStream(_ sender: XMPPStream, didReceive presence: XMPPPresence) {
        print(presence.showValue)
    }
    func xmppStream(_ sender: XMPPStream, didReceive iq: XMPPIQ) -> Bool {
        print(iq)
        return true
    }
    func xmppStream(_ sender: XMPPStream, didReceiveError error: DDXMLElement) {
        print(error)
    }
    func xmppStream(_ sender: XMPPStream, didSend message: XMPPMessage) {
        // Sent Successfully
        
    }
    //MARK: - Action Methods
    @IBAction func sendMessage(_ sender: Any) {
        let msg = self.tfOfMessage.text?.trimmingCharacters(in: .whitespaces)
        if (msg?.count)! > 0 {
            let senderJID = XMPPJID(string: contact!.jid)
            let xmppMsg = XMPPMessage(type: "chat", to: senderJID)
            xmppMsg.addBody(msg!)

            // uuid for Delivery Receipts
            let uuid = self.appDelegate.xmppStream.generateUUID
            xmppMsg.addAttribute(withName: "id", stringValue: uuid)
            self.appDelegate.xmppStream.send(xmppMsg)
//            let chatMessage = ChatMessage(message: msg!, sender: (appDelegate.xmppStream.myJID?.bare)!, date: Date().xmppDateTimeString, isIncoming: false)
//            self.arrOfMessages.append(chatMessage)
            self.tableView.reloadData()
            self.tfOfMessage.text = ""
        }
    }
    // MARK: - TF Delegates
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let xml = XMLElement(name: "message")
        xml.addAttribute(withName: "type", stringValue: "chat")
        xml.addAttribute(withName: "to", stringValue: self.contact?.jid ?? "")
        let msg = XMPPMessage(from: xml)
        msg.addChatState(.composing)
        self.appDelegate.xmppStream.send(xml)
        
        return true
    }
    //    func textFieldDidBeginEditing(_ textField: UITextField) {
    //        let xml = XMLElement(name: "message")
    //        xml.addAttribute(withName: "type", stringValue: "chat")
    //        xml.addAttribute(withName: "to", stringValue: self.contact?.jid ?? "")
    //        let msg = XMPPMessage(from: xml)
    //        msg.addChatState(.composing)
    //        self.appDelegate.xmppStream.send(xml)
    //    }
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        let xml = XMLElement(name: "message")
        xml.addAttribute(withName: "type", stringValue: "chat")
        xml.addAttribute(withName: "to", stringValue: self.contact?.jid ?? "")
        let msg = XMPPMessage(from: xml)
        msg.addChatState(.active)
        self.appDelegate.xmppStream.send(xml)
    }
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            UIView.animate(withDuration: 0.5) {
                self.tableView.scrollToBottom()
                self.bottomContraintOfViewOfMessage.constant =  -keyboardHeight
            }
            
        }
    }
    func resingTf() {
        self.tfOfMessage.resignFirstResponder()
        UIView.animate(withDuration: 0.5) {
            self.bottomContraintOfViewOfMessage.constant = 0
        }
    }
    //MARK: - Fetch Results Controller
    lazy var fetchResultsController: NSFetchedResultsController<NSFetchRequestResult> = {
        var fetchResult: NSFetchedResultsController<NSFetchRequestResult>?
        let xmppMsgStorage = XMPPMessageArchivingCoreDataStorage.sharedInstance()
        if let moc = xmppMsgStorage?.mainThreadManagedObjectContext{
            let entityDescription = NSEntityDescription.entity(forEntityName: "XMPPMessageArchiving_Message_CoreDataObject", in: moc)
            let predicateFormat = NSPredicate(format: "bareJidStr == %@", (self.contact?.jid)!)
            let sorting1: NSSortDescriptor = NSSortDescriptor.init(key: "timestamp", ascending: true)
            let sortDescriptors = [sorting1]
            let request = NSFetchRequest<NSFetchRequestResult>()
            request.entity = entityDescription
            request.predicate = predicateFormat
            request.sortDescriptors = sortDescriptors
            
            fetchResult = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "timestamp", cacheName: nil)
            fetchResult?.delegate = self
            
            do{
                try fetchResult?.performFetch()
            }catch{
                print("unable to fetch messages")
            }
        }
        return fetchResult ?? NSFetchedResultsController()
    }()
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.scrollToBottom()
        self.tableView.reloadData()
    }
    
}

