//
//  ViewController.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd on 07/09/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import UIKit
import KissXML
import XMPPFramework

class LoginViewController: UIViewController, XMPPStreamDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tfOfPass: UITextField!
    @IBOutlet weak var tfOfname: UITextField!
    
    //MARK: - Controller Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "login"
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: - Action Methods
    @IBAction func connectToXmpp(_ sender: Any) {
        let name = "\(tfOfname.text ?? "")@" + hostName
        let pass = tfOfPass.text!
        UserDefaults.standard.set(name, forKey: "userID")
        UserDefaults.standard.set(pass, forKey: "userPassword")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if appDelegate.connect() {
//            let contactsList = self.storyboard?.instantiateViewController(withIdentifier: "ContactsListViewController") as! ContactsListViewController
//            self.navigationController?.pushViewController(contactsList, animated: true)
            self.dismiss(animated: true, completion: nil)
        }else{
            print("Invalid Credentails")
        }
    }
    //MARK: - TF Methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


