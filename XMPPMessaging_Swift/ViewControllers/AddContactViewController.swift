//
//  AddContactViewController.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd. on 25/09/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import UIKit
import XMPPFramework

class AddContactViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var tfOfNickName: UITextField!
    @IBOutlet weak var tfOfName: UITextField!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    // MARK: - Controller
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Contact"
    }
    // MARK: - Action Methods

    @IBAction func addAction(_ sender: Any) {
        let name = self.tfOfName.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        let nickName = self.tfOfNickName.text?.trimmingCharacters(in: CharacterSet.whitespaces)

        if (name?.count)! > 0 && (nickName?.count)!     > 0{
            let jid = XMPPJID(string: String(describing: name!) + "@" + hostName)
//            appDelegate.xmppRoster.acceptPresenceSubscriptionRequest(from: jid!, andAddToRoster: true)
            appDelegate.xmppRoster.addUser(jid!, withNickname: nickName)
            appDelegate.xmppRoster.subscribePresence(toUser: jid!)
        }
     self.dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        resingTf()
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
