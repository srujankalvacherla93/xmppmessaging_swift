//
//  ContactsListViewController.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd on 14/09/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import UIKit
import XMPPFramework


class ContactsListViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, XMPPRosterDelegate, XMPPStreamDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    //    var arrOfContacts: [Contact] = Array()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//    var fetchResult: NSFetchedResultsController<NSFetchRequestResult>?
    
    //    let fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>
    
    
    // MARK: - Controller Cyclce
    override func viewDidLoad() {
        super.viewDidLoad()
//        appDelegate.delegate = self
        
        self.title = "Contacts"
        //        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "+", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.addContact))
        self.navigationController?.navigationBar.isHidden = true
        if (UserDefaults.standard.object(forKey: "userID") != nil) {
            if appDelegate.connect() {
//                self.title = appDelegate.xmppStream.myJID?.bare
                
//                let timer = Timer.scheduledTimer(timeInterval: 5.0,
//                                             target: self,
//                                             selector: #selector(self.testMessageArchiving),
//                                             userInfo: [ "foo" : "bar" ],
//                                             repeats: true)
                do{
                    try self.fetchResultsController.performFetch()
                }catch{
                    print("Fetch Failed")
                }
            }else{
                let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.navigationController?.present(loginVC, animated: true, completion: nil)
            }
        }else{
            let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.present(loginVC, animated: true, completion: nil)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        
        
    }
    @objc func addContact(){
        let addContactVC = self.storyboard?.instantiateViewController(withIdentifier: "AddContactViewController") as! AddContactViewController
        self.navigationController?.present(addContactVC, animated: true, completion: nil)
    }
    //MARK: - Table View Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchResultsController.sections?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return arrOfContacts.count
        if let sections = self.fetchResultsController.sections{
            if section < sections.count{
                let sectionInfo: NSFetchedResultsSectionInfo = (sections[section])
                return sectionInfo.numberOfObjects
            }
            return 0
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sections = self.fetchResultsController.sections
        if section < (sections?.count)!{
            let sectionInfo: NSFetchedResultsSectionInfo = (sections?[section])!
            switch sectionInfo.name
            {
            case "0":
                return "Avalibale"
            case "1":
                return "Away"
            default:
                return "Offline"
            }
        }
        return ""
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //        let contact = arrOfContacts[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        //        cell?.textLabel?.text = contact.name
        //        cell?.detailTextLabel?.text = "\(contact.isOnline)"
        let user = self.fetchResultsController.object(at: indexPath) as? XMPPUserCoreDataStorageObject
        cell?.textLabel?.text = user?.displayName
        self.configurePhotoForCell(cell!, user!)
//        cell?.imageView?.image = UIImage(named: "Default_ProfileCharacter")
        print(user?.unreadMessages as Any)
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let user = self.fetchResultsController.object(at: indexPath) as? XMPPUserCoreDataStorageObject{
            let chatRoom = self.storyboard?.instantiateViewController(withIdentifier: "ChatRoomViewController") as!  ChatRoomViewController
            let contact = Contact(name: user.displayName ?? "User Name", jid: user.jidStr ?? "", isOnline: false, pic: nil)
            chatRoom.contact = contact
            self.navigationController?.pushViewController(chatRoom, animated: true)
        }
    }
    
    func configurePhotoForCell(_ cell: UITableViewCell, _ user: XMPPUserCoreDataStorageObject){
        if user.photo != nil{
            cell.imageView?.image = user.photo
        }else{
//            cell.imageView?.image = UIImage.init(named: "Default_ProfileCharacter")
//            let photoData: NSData  = self.appDelegate.xmppvCardAvatarModule?.photoData(for: user.jid!)
            let photoData: Data? = self.appDelegate.xmppvCardAvatarModule?.photoData(for: user.jid!) ?? nil
            if photoData != nil{
                cell.imageView?.image = UIImage(data: photoData ?? Data())
            }else{
                cell.imageView?.image = UIImage.init(named: "Default_ProfileCharacter")
            }
        }
    }
    
    
    //MARK: - Chat delegates
    func didDisconnect() {
        print("Disconnected")
    }
    
    func buddyWentOnline(contact: Contact) {
        //        arrOfContacts.remove(at: <#T##Int#>)
        //        var latestContact = contact
        //        latestContact.isOnlie = true
        //        if arrOfContacts.contains(where: latestContact){
        //            arrOfContacts.
        //        }
        //        arrOfContacts.append(contact)
        //        self.tableView.reloadData()
        
    }
    
    func buddyWentOffline(contact: Contact) {
        
    }
    func receivedContacts(arrOfContacts: [Contact]) {
        //        self.arrOfContacts.removeAll()
        //        self.arrOfContacts = arrOfContacts
        //        self.tableView.reloadData()
    }
    // FetchResultsController Delegate Methods
    //    var fetchResultsController: NSFetchedResultsController {
    //        let moc: NSManagedObjectContext = self.appDelegate.managedob
    //    }
    //MARK: - Action Methods
    
    @IBAction func addContact(_ sender: Any) {
        let addContactVC = self.storyboard?.instantiateViewController(withIdentifier: "AddContactViewController") as! AddContactViewController
        self.navigationController?.present(addContactVC, animated: true, completion: nil)
    }
    //MARK: - Fetch Results Controller
    lazy var fetchResultsController: NSFetchedResultsController<NSFetchRequestResult> = {
        var fetchResult: NSFetchedResultsController<NSFetchRequestResult>?
        
            let moc: NSManagedObjectContext = self.appDelegate.managedObjectContext_roster
            if let entity: NSEntityDescription = NSEntityDescription.entity(forEntityName: "XMPPUserCoreDataStorageObject", in: moc){
                
                let sorting1: NSSortDescriptor = NSSortDescriptor.init(key: "sectionNum", ascending: true)
                let sorting2: NSSortDescriptor = NSSortDescriptor.init(key: "displayName", ascending: true)
                let sortDescriptors = [sorting1, sorting2]
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
                fetchRequest.entity = entity
                fetchRequest.sortDescriptors = sortDescriptors
                fetchRequest.fetchBatchSize = 10
                
                fetchResult = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: moc, sectionNameKeyPath: "sectionNum", cacheName: nil)
                
                fetchResult?.delegate = self
                do{
                    try fetchResult?.performFetch()
                }catch{
                    print("unable to fetch form core data")
                }
            }
        return fetchResult ?? NSFetchedResultsController()
    }()
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.reloadData()
    }
    //MARK: - Test Message Archive
    @objc func testMessageArchiving() {
        let storage = XMPPMessageArchivingCoreDataStorage.sharedInstance()
        
        if let moc = storage?.mainThreadManagedObjectContext{
            let entityDescription = NSEntityDescription.entity(forEntityName: "XMPPMessageArchiving_Message_CoreDataObject", in: moc)
            let request = NSFetchRequest<NSFetchRequestResult>()
            request.entity = entityDescription
            do{
                
                let messages = try moc.fetch(request) as? Array<XMPPMessageArchiving_Message_CoreDataObject>
//                print("Messages: \(messages)")
                printData(messages!)
            }catch{
                print("Unable to exicute")
            }
            
        }
    }
    func printData(_ messages: Array<XMPPMessageArchiving_Message_CoreDataObject>)  {
        for message in messages {
            /*
             NSLog(@"messageStr param is %@",message.messageStr);
             NSXMLElement *element = [[NSXMLElement alloc] initWithXMLString:message.messageStr error:nil];
             NSLog(@"to param is %@",[element attributeStringValueForName:@"to"]);
             NSLog(@"NSCore object id param is %@",message.objectID);
             NSLog(@"bareJid param is %@",message.bareJid);
             NSLog(@"bareJidStr param is %@",message.bareJidStr);
             NSLog(@"body param is %@",message.body);
             NSLog(@"timestamp param is %@",message.timestamp);
             NSLog(@"outgoing param is %d",[message.outgoing intValue]);
             */
            print("Message: \(message.messageStr ?? "No Message")")
            let element = XMLElement.init(name: message.messageStr)
            print("to param is: \(String(describing: element.attribute(forName: "to")))")
        }
    }

    
    
}
