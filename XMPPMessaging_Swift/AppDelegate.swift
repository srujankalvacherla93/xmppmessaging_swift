//
//  AppDelegate.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd on 07/09/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import UIKit
import CoreData
import CocoaLumberjack
import XMPPFramework
import SKAlert


public let hostName = "localhost"
public let hostIPAddress = "172.31.98.137"
//public let hostIPAddress = "192.168.2.9"
public let hostPort: UInt16 = 5222

var xmppController: XMPPController!

//protocol ChatDelegate {
//    func buddyWentOnline(contact: Contact)
//    func buddyWentOffline(contact: Contact)
//    func receivedContacts(arrOfContacts: [Contact])
//    func didDisconnect()
//}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, XMPPStreamDelegate, XMPPRosterDelegate{
    
    var window: UIWindow?
    var navigationController : UINavigationController?
    var selectedStoryboard : UIStoryboard?
    
    // XMPP Delegates
//    var delegate:ChatDelegate! = nil
    let xmppStream = XMPPStream()
    var xmppRosterStorage = XMPPRosterCoreDataStorage()
    var xmppRoster: XMPPRoster
    var contactsVC: ContactsListViewController?
    var managedObjectContextCapabilities: NSManagedObjectContext?
    var xmppMessageArchivingStorage: XMPPMessageArchivingStorage?
    var xmppMessageArchivingModule: XMPPMessageArchiving?
    var xmppReconnect: XMPPReconnect?
    var xmppvCardTempModule: XMPPvCardTempModule?
    var xmppvCardAvatarModule: XMPPvCardAvatarModule?
    var xmppvCardStorage: XMPPvCardCoreDataStorage?
    var xmppMessageDeliveryRecipts: XMPPMessageDeliveryReceipts?
    
    
    override init() {
        // Roster
        xmppRoster = XMPPRoster(rosterStorage: xmppRosterStorage)
        // Message Storage
        xmppMessageArchivingStorage = XMPPMessageArchivingCoreDataStorage.sharedInstance()
        xmppMessageArchivingModule = XMPPMessageArchiving.init(messageArchivingStorage: xmppMessageArchivingStorage)
        // Vcard Storage
        xmppvCardStorage = XMPPvCardCoreDataStorage.sharedInstance()
        xmppvCardTempModule = XMPPvCardTempModule(vCardStorage: xmppvCardStorage!)
        xmppvCardAvatarModule = XMPPvCardAvatarModule(vCardTempModule: xmppvCardTempModule!)
        // Delivery Reports
        xmppMessageDeliveryRecipts = XMPPMessageDeliveryReceipts(dispatchQueue: DispatchQueue.main)
        // Reconnect
        xmppReconnect = XMPPReconnect()
        super.init()
        //
        self.documentDir()
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        DDLog.add(DDTTYLogger.sharedInstance, with: DDLogLevel.all)
        //[DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:XMPP_LOG_FLAG_SEND_RECV];
//        DDLog.add(DDTTYLogger.sharedInstance, with: XMPP_LOG_FLAG_SEND_RECV)
        
        // XMPP Stream
        self.setupStream()
        //
        window = UIWindow(frame: UIScreen.main.bounds)
        selectedStoryboard =  UIStoryboard.init(name: "Main", bundle: nil)
        if let window = window{
            if UserDefaultStore.isLogined == nil{
                UserDefaultStore.isLogined = "0"
            }
            contactsVC = self.selectedStoryboard?.instantiateViewController(withIdentifier: "ContactsListViewController") as? ContactsListViewController
            navigationController = UINavigationController(rootViewController: contactsVC!)
            if #available(iOS 11.0, *) {
                navigationController?.navigationBar.prefersLargeTitles = true
            } else {
                // Fallback on earlier versions
            }
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        disconnect()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.connect()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "XMPPMessaging_Swift")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    //MARK: - XMPP Private Methods
    private func setupStream() {
        
        // Set Host Name & Port
        self.xmppStream.hostName = hostIPAddress
        self.xmppStream.hostPort = hostPort
        
        /// XMPP Message Archive Modules
        self.xmppMessageArchivingModule?.clientSideMessageArchivingOnly = true // Store locally
        ///
        self.xmppRoster.autoFetchRoster = true
        self.xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = true
        /// Dont clear DB
        self.xmppRosterStorage.autoRemovePreviousDatabaseFile = false
        self.xmppRoster.autoClearAllUsersAndResources = false
        // Delivery Receipts
        self.xmppMessageDeliveryRecipts?.autoSendMessageDeliveryReceipts = true
        self.xmppMessageDeliveryRecipts?.autoSendMessageDeliveryRequests = true
        
        /// Activating Features for stream
        self.xmppRoster.activate(self.xmppStream)
        self.xmppMessageArchivingModule?.activate(self.xmppStream)
        self.xmppReconnect?.activate(self.xmppStream)
        self.xmppvCardTempModule?.activate(self.xmppStream)
        self.xmppvCardAvatarModule?.activate(self.xmppStream)
        self.xmppMessageDeliveryRecipts?.activate(self.xmppStream)
        ///
        self.xmppStream.addDelegate(self, delegateQueue: DispatchQueue.main)
        self.xmppRoster.addDelegate(self, delegateQueue: DispatchQueue.main)
        xmppMessageArchivingModule?.addDelegate(self, delegateQueue: DispatchQueue.main)
    }
    private func goOnline() {
        let presence = XMPPPresence()
        let domain = xmppStream.myJID?.domain
        if domain == "gmail.com" || domain == "gtalk.com" || domain == "talk.google.com" || domain == hostName{
            let priority = DDXMLElement.element(withName: "priority", stringValue: "24") as! DDXMLElement
            presence.addChild(priority)
        }
        xmppStream.send(presence)
    }
    
    private func goOffline() {
        let presence = XMPPPresence(type: "unavailable")
        xmppStream.send(presence)
    }
    
    func connect() -> Bool {
        if !xmppStream.isConnected {
            let jabberID = UserDefaults.standard.string(forKey: "userID")
            let myPassword = UserDefaults.standard.string(forKey: "userPassword")
            
            if !xmppStream.isDisconnected {
                return true
            }
            if jabberID == nil && myPassword == nil {
                return false
            }
            
            xmppStream.myJID = XMPPJID(string: jabberID!)
            
            do {
                try xmppStream.connect(withTimeout: XMPPStreamTimeoutNone)
                print("Connection success")
                return true
            } catch {
                print("Something went wrong!")
                return false
            }
        } else {
            return true
        }
    }
    func disconnect() {
        goOffline()
        xmppStream.disconnect()
    }
    
    // MARK: - Stream Delegates
    func xmppStreamDidConnect(_ stream: XMPPStream) {
        print("Stream: Connected")
        do{
            try xmppStream.authenticate(withPassword: UserDefaults.standard.string(forKey: "userPassword")!)
        }catch{
            print("Not Authenticate")
            let loginVC = self.selectedStoryboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.navigationController?.present(loginVC, animated: true, completion: nil)
        }
    }
    func xmppStreamDidDisconnect(_ sender: XMPPStream, withError error: Error?) {
        print("Stream did disconnect")

        _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { (_) in
            self.connect()
        })
    }
    
    func xmppStream(_ sender: XMPPStream, didNotRegister error: DDXMLElement) {
        print("didNotRegister")
    }
    
    func xmppStreamDidAuthenticate(_ sender: XMPPStream) {
        print("xmppStreamDidAuthenticate")
//        self.getRosterList()
        goOnline()
    }
    func xmppStream(_ sender: XMPPStream, didNotAuthenticate error: DDXMLElement) {
        print("Not Authenticate")
        let loginVC = self.selectedStoryboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.present(loginVC, animated: true, completion: nil)

    }
    
    func xmppStream(_ sender: XMPPStream, didReceive iq: XMPPIQ) -> Bool {
        print("Did receive IQ")
        
//        var arrOfRoster: [Contact] = Array()
//        let query = iq.element(forName: "query" , xmlns: "jabber:iq:roster")
//        if (query != nil){
//            let arrOfitem = query?.elements(forName: "item")
//            for friend in arrOfitem ?? []{
//                let jid = friend.attribute(forName: "jid")?.stringValue ?? ""
//                var name = friend.attribute(forName: "name")?.stringValue ?? ""
//                if name == ""{
//                    name = jid
//                }
//                let contact = Contact(name: name, jid: jid, isOnline: false)
//                arrOfRoster.append(contact)
//            }
//            if self.delegate != nil{
//                delegate.receivedContacts(arrOfContacts: arrOfRoster)
//            }
//        }
        return false
    }
    func xmppStream(_ sender: XMPPStream, didReceive presence: XMPPPresence) {
        /*
        let presenceType = presence.type
        let myUsername = sender.myJID?.user
        let presenceFromUser = presence.from?.user
        let presenceState = presence.status
        
        print("\(presenceFromUser ?? "presenceFromUser")  is \(presenceType ?? "") state \(presenceState ?? "")")
        
        if presenceFromUser != myUsername {
            print("Did receive presence from \(String(describing: presenceFromUser))")
            if delegate != nil{
                if presenceType == "available" {
                    let contact1 = Contact(name: presenceFromUser ?? "" , jid: "\(String(describing: presenceFromUser))@\(hostName)", isOnline: true)
                    delegate.buddyWentOnline(contact: contact1)
                } else if presenceType == "unavailable" {
                    let contact1 = Contact(name: presenceFromUser ?? "" , jid: "\(String(describing: presenceFromUser))@\(hostName)", isOnline: false)
                    delegate.buddyWentOnline(contact: contact1)
                }
            }
        }
        */
    }
    // MARK: - Message Delegates
    
    func xmppStream(_ sender: XMPPStream, didReceive message: XMPPMessage) {
        print("Did receive message \(String(describing: message))")

        if message.isChatMessage{
//            let user = xmppRosterStorage.user(for: message.from, xmppStream: self.xmppStream, managedObjectContext: self.managedObjectContext_roster)
//            let body = message.element(forName: "body")?.stringValue ?? ""
//            let displayName = user?.displayName ?? ""
//            if UIApplication.shared.applicationState == .active{
//                _ = SKAlert().showAlertWithTwoButtons(displayName, subTitle: body, okCompletionHandler: {
//
//                }, cancelCompletionHandler: {
//
//                })
//            }
        }
    }
    func xmppStream(_ sender: XMPPStream, didSend message: XMPPMessage) {
        print("Did send message \(String(describing: message))")
    }
    
    
    // MARK: - Roster Delegates
    func xmppRoster(_ sender: XMPPRoster, didReceiveRosterItem item: DDXMLElement) {
        print("Did receive Roster item")
//        self.xmppRosterStorage.handleRosterItem(item, xmppStream: self.xmppStream)
    }
    var managedObjectContext_roster: NSManagedObjectContext {
        return  self.xmppRosterStorage.mainThreadManagedObjectContext
    }
    func getRosterList() {
        
        do{
            let query = try XMLElement(xmlString: "<query xmlns='jabber:iq:roster'/>")
            let iq = XMLElement(name: "iq")
            iq.addAttribute(withName: "type", stringValue: "get")
            iq.addAttribute(withName: "id", stringValue: "\(xmppStream.myJID?.user ?? "")")
            iq.addAttribute(withName: "from", stringValue: "\(xmppStream.myJID?.bare ?? "")")
            iq.addChild(query)
            self.xmppStream.send(iq)
        }catch{
            print("Unable to fetch Friends list")
        }
    }
    func xmppRoster(_ sender: XMPPRoster, didReceivePresenceSubscriptionRequest presence: XMPPPresence) {
        // Someone has added to their roster and requested to perform action to accept or reject
        // By using acceptPresenceSubscriptionRequestFrom (or) rejectPresenceSubscriptionRequestFrom
        
        //        let _: XMPPUserCoreDataStorageObject = xmppRosterStorage.user(for: presence.from!, xmppStream: xmppStream, managedObjectContext: self.managedObjectContext_roster)
//        let user = self.xmppRosterStorage.user(for: presence.from!, xmppStream: self.xmppStream, managedObjectContext: self.managedObjectContext_roster)
        _ = SKAlert().showAlertWithTwoButtons("Friend Request From", subTitle: presence.fromStr ?? "Unkown User", okCompletionHandler: {
            self.xmppRoster.acceptPresenceSubscriptionRequest(from: presence.from!, andAddToRoster: true)
        }, cancelCompletionHandler: {
            
        })
    }
    //MARK: - Others
    func documentDir(){
        
        var paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as [String]
        
//        return paths[0]
        print("Path for DB: \(paths[0])")
    }
    
    
    
}


