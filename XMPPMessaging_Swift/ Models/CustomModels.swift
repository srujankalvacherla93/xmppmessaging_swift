//
//  CustomModels.swift
//  XMPPMessaging_Swift
//
//  Created by SMSCountry Networks Pvt. Ltd. on 16/10/18.
//  Copyright © 2018 SMSCountry Networks Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit

public struct ChatMessage {
    var message: String?
    var sender: String?
    var timestamp: Date?
    var isOutgoing: Bool?
    var isComposing: Bool?
}
public struct UserObject {
    let name: String
    let jid: String
}
public struct Contact {
    let name: String
    let jid: String
    var isOnline: Bool
    var pic: UIImage?
    //    let profilePic: String
    //    let lastMessage: String
    //    let lastMessageTime: String
}
